package com.tonino;

public class Child extends Human {
    private String occupation;

    public Child(String name, int age, String gender, String occupation) {
        super(name, age, gender);
        this.occupation = occupation;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public String getGender() {
        return super.getGender();
    }
}
