package com.tonino;

public class Main {
    public static void main(String[] args) {
        Child child1 = new Child("John Doe", 30, "M", "Toddler");
        Child child2 = new Child("Jane Doe", 32, "F", "Coddler");
        System.out.println(child1.getName());
        System.out.println(child2.talk());
    }
}
